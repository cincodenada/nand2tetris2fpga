`timescale 1ms/10us

module testbench;

    reg [15:0] rom[0:100];
    reg [15:0] ram[0:32767];

    reg [15:0] inM;
    reg [15:0] instruction;
    reg reset,clk;

    wire [15:0] outM;
    wire writeM;
    wire [14:0] addressM;
    wire [14:0] pc;

    localparam period=20;

    HackCPU UUT (
        .instruction(instruction),.reset(reset),.clk(clk),
        .inM(inM),.outM(outM),.writeM(writeM),.addressM(addressM),
        .pc(pc)
    );


    integer i;
    integer c;
    initial begin
        $monitor({
            "clk=%d\n",
            "instruction=%b\n",
            "pc=%h\n",
            "outM=%h\n",
            "writeM=%h\n",
            "addressM=%h\n",
            "regA=%h\n",
            "regD=%h\n",
            "alux=%b\n",
            "aluy=%b\n",
            "nx=%b\n",
            "ny=%b\n",
            "zx=%b\n",
            "zy=%b\n",
            "\n"},
        clk, instruction, pc,
        outM, writeM, addressM,
        UUT.ARegister.out, UUT.DRegister.out,
        UUT.ALU.x, UUT.ALU.y,
        UUT.ALU.nx, UUT.ALU.ny,
        UUT.ALU.zx, UUT.ALU.zy);

        $readmemb("test.hack", rom);
        $readmemh("ram.hex", ram);
        instruction=0;
        reset=1;
        clk=0; #period; clk=1; #period;
        reset=0;
        clk=0; #period; clk=1; #period;
        while (1) begin
            instruction=rom[pc];
            if (writeM) ram[addressM] <= outM;
            inM <= ram[addressM];
            clk=0;
            $display("ram[%h]=",UUT.ARegister.out,ram[UUT.ARegister.out]);
            #period;
            clk=1;
            inM <= ram[addressM];
            $display("ram[%h]=",UUT.ARegister.out,ram[UUT.ARegister.out]);
            #period;
            
            if(rom[pc-1] === pc-1 && rom[pc] === 16'b1110101010000111) begin
                for(i=0;i<10;i=i+1) begin
                    $display("mem[%d]=%h",i,ram[i]);
                end
                /*
                for(i=16384;i<24576;i=i+32) begin
                    for(c=0;c<32;c++) begin
                        $write("%b",ram[i+c]);
                    end
                    $display("");
                end
                */
                $finish;
            end
        end
    end
endmodule


