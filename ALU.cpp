#include <verilated.h>

#include <algorithm>
#include <fstream>
#include <iostream>
#include <string>

#include "VHackALU.h"

VHackALU* alu;  // Create instance

vluint64_t main_time = 0;

double sc_time_stamp() { return main_time; }

int getnum(std::string& str, std::string delim) {
    size_t pos = str.find(delim);
    int val = -1;
    if (pos != std::string::npos) {
        std::string token = str.substr(0, pos);
        token.erase(std::remove(token.begin(), token.end(), ' '), token.end());
        if (token.length()) {
            val = std::stoi(token, nullptr, 2);
        }
        str.erase(0, pos + delim.length());
    }
    return val;
}

int main(int argc, char** argv) {
    Verilated::commandArgs(argc, argv);  // Remember args

    alu = new VHackALU;  // Create instance

    std::fstream fs;
    fs.open("02/ALU.cmp");
    std::string line;
    std::getline(fs, line);
    size_t linenum = 1;
    for (; std::getline(fs, line);) {
        size_t pos = 0;
        getnum(line, "|");
        alu->x = getnum(line, "|");
        alu->y = getnum(line, "|");
        alu->zx = getnum(line, "|");
        alu->nx = getnum(line, "|");
        alu->zy = getnum(line, "|");
        alu->ny = getnum(line, "|");
        alu->f = getnum(line, "|");
        alu->no = getnum(line, "|");

        int expected_out = getnum(line, "|");
        int expected_zr = getnum(line, "|");
        int expected_ng = getnum(line, "|");

        alu->eval();
        if (alu->out != expected_out) {
            std::cerr << "Mismatch on line " << linenum << "\n";
            std::cerr << "Expected " << expected_out  //
                      << ", got " << alu->out;
        } else {
        }

        linenum++;
    }

    vluint16_t x;
    vluint16_t y;
    std::string flags;

    std::cin >> x;
    std::cin >> y;
    std::cin >> flags;
    int flagval = std::stoi(flags, nullptr, 2);

    alu->x = x;  // Set some inputs
    alu->y = y;  // Set some inputs
    alu->zx = flagval & 0b100000;
    alu->nx = flagval & 0b010000;
    alu->zy = flagval & 0b001000;
    alu->ny = flagval & 0b000100;
    alu->f = flagval & 0b000010;
    alu->no = flagval & 0b000001;

    alu->eval();

    std::cout << alu->out << std::boolalpha << //
        " (" << bool(alu->zr) << "," << bool(alu->ng) << ")\n";

    alu->final();  // Done simulating
}
