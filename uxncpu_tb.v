`timescale 1ms/10us

module testbench;

    reg [15:0] rom[0:100];
    reg [15:0] ram[0:100];
    reg [15:0] stack[0:255];

    reg [7:0] instruction;
    reg reset,clk;
    wire [7:0] pc;
    reg stackAddr1, stackAddr2;
    reg stackIn1, stackIn2;
    reg stackOut1, stackOut2;
    reg stackLoad;

    localparam period=20;

    HackUxnCPU UUT (
        .instruction(instruction),.clk(clk),
        .pc(pc),
        .stackIn1(stackIn1),.stackIn2(stackIn2),
        .stackOut1(stackOut1),.stackOut2(stackOut2),
        .stackAddr1(stackAddr1),.stackAddr2(stackAddr2),
        .stackLoad(stackLoad)
    );

    always @ (posedge clk) begin
        stackIn1 <= stack[stackAddr1];
        stackIn2 <= stack[stackAddr2];
        if(stackLoad) begin
            stack[stackAddr1] <= stackOut1;
            stack[stackAddr2] <= stackOut2;
        end
    end

    integer i;
    integer c;
    initial begin
        $monitor({
            "clk=%d\n",
            "instruction=%b\n",
            "pc=%h\n",
            "\n"},
        clk, instruction, pc);

        $readmemb("test.hack", rom);
        $readmemh("ram.hex", ram);
        instruction=0;
        reset=1;
        clk=0; #period; clk=1; #period;
        reset=0;
        clk=0; #period; clk=1; #period;
        while (1) begin
            instruction=rom[pc];
            clk=0;
            #period;
            clk=1;
            #period;
            
            if(UUT.Stack.t === pc && rom[pc] === 8'h0c) begin
                for(i=UUT.Stack.head;i>=0;i=i-1) begin
                    $display("stack[%d]=%h",i,stack[i]);
                end
                /*
                for(i=16384;i<24576;i=i+32) begin
                    for(c=0;c<32;c++) begin
                        $write("%b",ram[i+c]);
                    end
                    $display("");
                end
                */
                $finish;
            end
        end
    end
endmodule


