use clap::{Arg, App};
use std::fs;

fn main() {
    let matches = App::new("Hack Utils")
        .arg(Arg::with_name("command"))
        .arg(Arg::with_name("infile"))
        .get_matches();

    let infile = matches.value_of("infile").expect("No input file!");
    let contents = fs::read_to_string(infile)
        .expect("Couldn't read file!");

    for line in contents.split_ascii_whitespace() {
        match &line[0..1] {
            "0" => {
                println!("@{}", isize::from_str_radix(&line[1..],2).unwrap())
            },
            "1" => {
                let a_or_m = match &line[3..4] {
                    "0" => "A",
                    "1" => "M",
                    _ => panic!("nope")
                };
                let comp = str::replace(match &line[4..10] {
                    "101010" => "0",
                    "111111" => "1",
                    "111010" => "-1",
                    "001100" => "D",
                    "110000" => "X",
                    "001111" => "-D",
                    "110011" => "-X",
                    "011111" => "D+1",
                    "110111" => "X+1",
                    "001110" => "D-1",
                    "110010" => "X-1",
                    "000010" => "D+X",
                    "010011" => "D-X",
                    "000111" => "X-D",
                    "000000" => "D&X",
                    "010101" => "D|X",
                    _ => "Invalid comp"
                }, "X", a_or_m);
                let dest = match &line[10..13] {
                    "000" => "",
                    "001" => "M=",
                    "010" => "D=",
                    "011" => "MD=",
                    "100" => "A=",
                    "101" => "AM=",
                    "110" => "AD=",
                    "111" => "AMD=",
                    _ => "<Invalid dest>"
                };
                let jump = match &line[13..16] {
                    "000" => "",
                    "001" => ";JGT",
                    "010" => ";JEQ",
                    "011" => ";JGE",
                    "100" => ";JLT",
                    "101" => ";JNE",
                    "110" => ";JLE",
                    "111" => ";JMP",
                    _ => "<Invalid jump>"
                };

                println!("{}{}{}",dest,comp,jump)
            },
            _ => panic!("Invalid binary string!")
        }
    }
    
}
