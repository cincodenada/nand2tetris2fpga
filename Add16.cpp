#include <verilated.h>

#include <fstream>
#include <iostream>
#include <string>
#include <algorithm>

#include "VHackAdd16.h"

VHackAdd16* adder;  // Create instance

vluint64_t main_time = 0;

double sc_time_stamp() { return main_time; }

int getnum(std::string& str, std::string delim) {
    size_t pos = str.find(delim);
    int val = -1;
    if (pos != std::string::npos) {
        std::string token = str.substr(0, pos);
        token.erase(std::remove(token.begin(), token.end(), ' '), token.end());
        if(token.length()) {
            val = std::stoi(token, nullptr, 2);
        }
        str.erase(0, pos + delim.length());
    }
    return val;
}

int main(int argc, char** argv) {
    Verilated::commandArgs(argc, argv);  // Remember args

    adder = new VHackAdd16;  // Create instance

    std::fstream fs;
    fs.open("02/Add16.cmp");
    std::string line;
    std::getline(fs, line);
    size_t linenum = 1;
    for (; std::getline(fs, line);) {
        size_t pos = 0;
        getnum(line, "|");
        adder->a = getnum(line, "|");
        adder->b = getnum(line, "|");
        int expected_out = getnum(line, "|");

        adder->eval();
        if (adder->out != expected_out) {
            std::cerr << "Mismatch on line " << linenum << "\n";
            std::cerr << "Expected " << expected_out  //
                      << ", got " << adder->out;
        } else {
            std::cout << "Success:" << adder->a //
                      << "+" << adder->b //
                      << "=" << adder->out << "\n";
        }

        linenum++;
    }

    vluint16_t a;
    vluint16_t b;

    std::cin >> a;
    std::cin >> b;

    adder->a = a;  // Set some inputs
    adder->b = b;  // Set some inputs

    adder->eval();

    std::cout << adder->out;

    adder->final();  // Done simulating
}
