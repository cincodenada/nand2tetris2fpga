module HackNand(input a, input b, output out);
    nand(out, a, b);
endmodule

/*
module DFF(input D, output Q, input CLK);
    wire outa;
    wire outb;
    nand(outa, ina, outb);
    nand(outb, inb, outa);

    nand(ina, D, CLK);
    nand(inb, CLK, notd);
    nand(notd, D, D);
    assign Q=outa;
endmodule
*/
module DFF (input D,
              input CLK,
              output reg Q);

    always @ (negedge CLK)
      Q <= D;
endmodule

module HackDFF(input in, output out, input clk);
    DFF dff (.D(in), .Q(out), .CLK(clk));
endmodule

