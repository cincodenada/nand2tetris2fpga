import fileinput

cmds = {
  'inc': 0b10100000,
  'dec': 0b00100000,
  'read': 0b00001000,
  'write': 0b00010000,
  'rst': 0b00100001,
  'push': 0b11111010,
  'nop': 0b00000000,
  'jmp': 0b00000000
}

paramvals = {
  'inc': 1,
}

def make_cmd(cmd):
  parts = cmd.split('(')

  paramval = 0
  params = []
  if len(parts) > 1:
    params = parts[1].replace(')','').split(',')
    if(len(params) == 2):
      paramval += 4*(params[0]=='n')
      paramval += 2*(params[1]=='t')
    else:
      if params[0] in paramvals:
        paramval += paramvals[params[0]]
      else:
        paramval += int(params[0], base=16)

  return cmds[parts[0]] + paramval

def fill_codes(codes):
  for i in range(max(codes.keys())):
    subcodes = codes.get(i, [])
    for j in range(len(subcodes)):
      yield subcodes[j]
    for j in range(len(subcodes),8):
      yield 0

def dump_hack(codes):
  for c in fill_codes(codes):
    print "{:016b}".format(c)

def dump_tst(codes):
  print "set enabled 0, set mcload 1;"
  for addr, c in enumerate(fill_codes(codes)):
    print "set mcaddr {}, set mcval %X{:04x}, tick, tock;".format(
      addr, c)
  print "set enabled 1, set mcload 0;"


def dump_hex(codes):
  for i in range(max(codes.keys())):
    if i in codes:
      hexcmds = ["{0:x}".format(c) for c in codes[i]]
    else:
      hexcmds = []
    print "{:0<16}".format(
      "".join(hexcmds)
    )

codes = {}
for line in fileinput.input():
  parts = line.split()
  idx = int(parts[0], base=16)
  codes[idx] = [
    make_cmd(c) for c in parts[1:]
  ]
dump_tst(codes)

