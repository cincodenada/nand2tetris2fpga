import fileinput
 
cmds = [
    'BRK','LIT','---','POP','DUP','SWP','OVR','ROT'
]
headinc = [ 0,1,-1,1,0,0,0 ]
head = 0

def dump_tst(op):
  global head
  print("set opcode %X{:02x}, set head {};".format(op, head))
  print("while incROM = 0 { tick, eval; tock, eval; }")
  if op in headinc:
    head += headinc[op]

for line in fileinput.input():
  for tok in line.split():
    if tok[0] == '#':
      op = int(tok[1:], base=16)
    elif tok in cmds:
      op = cmds.index(tok)
    else:
      raise 'Token {} not recognized!'.format(tok)
    
    dump_tst(op)
