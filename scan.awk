BEGIN { print "WIDTHS_START" }
END { print "WIDTHS_END" }
/^CHIP/ {
  chip=$2
  state="modhead"
  next
}
/PARTS/ {
  state="modbody"
  next
}
/^}/ {
  state = "done"
}
/IN/ { porttype="IN"; }
/OUT/ { porttype="OUT"; }

state == "modhead" && length($0) {
  gsub(/(IN|OUT) ?/, "")
  gsub(/\/\/.*/, "")
  patsplit($0, ioports, /\w+(\[[0-9]+\])?/, glue)
  for(i=1;i<=length(ioports);i++) {
    split(ioports[i], parts, /[\[\]]/)
    print chip,porttype,parts[1],parts[2]
  }
}

state == "modbody" && /\w+\(/ {
  partname=gensub(/.*\s+(\w+)\(.*/,"\\1",1,$0)
  containedby[partname][chip]=1
}

function printParents(chip) {
  print chip
  if(chip in containedby) {
    for(parent in containedby[chip]) {
      printParents(parent)
    }
  }
}

END {
  print "CLOCKED_START"
  printParents("DFF")
  printParents("GowinRAM1K")
  print "CLOCKED_END"
}
