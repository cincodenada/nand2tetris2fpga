%.vvp: %_tb.v %.v
	iverilog -o $@ $^

hackcpu.v: parts.txt convert.awk ports.dat Makefile header.v footer.v
	grep -v -E "0.*(RAM|Memory|Computer)" parts.txt | \
	grep -v "Gowin" | \
	xargs awk -f convert.awk ports.dat | \
		cat header.v - footer.v > hackcpu.v

uxncpu.v: parts.txt convert.awk ports.dat Makefile header.v footer.v
	grep -E "(0[1234]|uxn|eightbit)" parts.txt | \
	xargs awk -f convert.awk ports.dat | \
		cat header.v - footer.v > uxncpu.v

ports.dat: */*.hdl scan.awk parts.txt Makefile
	xargs -a parts.txt \
		awk -f scan.awk > ports.dat

parts.txt: */*.hdl Makefile
	find . -type f -name "*.hdl" \
		-not -path "*/demo/*" \
		> parts.txt

%.hex: %.hack
	echo 'obase=10;ibase=2;scale=4' | \
		cat - $^ | bc | \
		awk '{printf "%04x\n", $$0}' \
		> $@

EXCLUDE_Add16 := "Inc|ALU|CPU|"
EXCLUDE_ALU := "CPU|"
EXCLUDE_CPU := ""

sim: sim_Add16 sim_ALU sim_CPU

sim_%: %.cpp %.sim.v Hack%.h
	verilator $*.cpp $*.sim.v \
		--cc \
			-Wno-fatal \
			-CFLAGS --std=c++17 \
		--exe \
		--top-module Hack$* \
		--Mdir obj_$*
	(cd obj_$* && make -f VHack$*.mk)
	mv obj_$*/VHack$* $@

Hack%.h: obj_%/ reflect.awk
	awk -f reflect.awk obj_$*/VHack$*.h > $@

obj_%/: %.sim.v %.cpp
	verilator $^ \
		--cc \
			-Wno-fatal \
			-CFLAGS --std=c++17 \
		--top-module Hack$* \
		--Mdir $@

%.sim.v: parts.txt convert.awk ports.dat Makefile header.v footer.v
	grep -v -E "("$(EXCLUDE_$*)"RAM|Memory|Computer)" parts.txt | \
	xargs awk -f convert.awk ports.dat | \
		cat header.v - footer.v > $@

