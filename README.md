This project is, well, probably best described by [this Mastodon
post][yeshahahayes].

On the surface, it is an effort to convert a [Nand2Tetris][n2t] project into
actual HDL (Verilog) so that it can be programmed onto an FPGA. I'm making
progress! I also am secretly working on building a native [uxn][uxn] CPU built
on top of a Nand2Tetris base, but ssh, don't tell Devine until I get it working
;)

But it's also turned into me having fun misusing programming languages in
terrible ways, just because I can. Slapdash mixtures of modern C++ and
old-style C string handling, a huge bundle of Awk stretched far beyond what it
should ever be used for, and hey, why not throw some Rust in there while we're
at it?

Oh, and of course everything is glued together with a Makefile that makes much
use of piping things around to grep, xargs, and bc, and features recursive
variable expansion.

So, like, please for the love of anyone who may have to read your code in the
future, _don't_ emulate this coding style, it is a hot mess that I wouldn't
wish on my worst enemy. But it's my hot mess and I'm having fun, and actually
kinda accomplishing my end goals!

The other thing is that, out of respect for the Nand2Tetris folks' wishes, I
have excised the Nand2Tetris HDL files from this repo. You'll have to supply
your own implementation of a Nand2Tetris CPU if you want to actually run this -
but it should work with any implementation, I think!

Just drop these files into your Nand2Tetris project folder and have at it.

[yeshahahayes]: https://cybre.space/@cincodenada/106163727110276922
[n2t]: https://www.nand2tetris.org/
[uxn]: https://wiki.xxiivv.com/site/uxn.html
