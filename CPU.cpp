#include <verilated.h>

#include <algorithm>
#include <cassert>
#include <fstream>
#include <iostream>
#include <map>
#include <string>
#include <vector>

#include "HackCPU.h"

HackCPU* cpu;
class ColSpec;
std::vector<ColSpec> cols;

vluint64_t main_time = 0;

double sc_time_stamp() { return main_time; }

std::map<char, size_t> bases = {{'B', 2}, {'D', 10}, {'S', 10}};

void printbin(char* dest, size_t width, int number) {
  int mask = 1 << (width - 1);
  while (mask) {
    *dest = (number & mask) ? '1' : '0';
    dest++;
    mask >>= 1;
  }
  *dest = '\0';
}

class ColSpec {
 public:
  ColSpec(char* spec) {
    std::cerr << "Parsing: " << spec << "\n";
    char* tok = strtok(spec, "%");
    name.assign(tok);

    tok = strtok(nullptr, ".");
    type = *tok;
    tok++;
    lpad = std::stoi(tok, nullptr, 10);
    tok = strtok(nullptr, ".");
    width = std::stoi(tok, nullptr, 10);
    tok = strtok(nullptr, ".");
    rpad = std::stoi(tok, nullptr, 10);
  }

  void outval(int value, char* dest, size_t sz) {
    const char* suffix;
    switch (type) {
      case 'S':
        suffix = (value % 2) ? "+" : "";
        value /= 2;
        snprintf(dest, sz, "%d%s", value, suffix);
        break;
      case 'B':
        assert(width <= sz);
        printbin(dest, width, value);
        break;
      case 'D':
        snprintf(dest, sz, "%d", value);
        break;
    }
  }
  void outval(std::string& value, char* dest, size_t sz) {
    outval(value.c_str(), dest, sz);
  }
  void outval(const char* value, char* dest, size_t sz) {
    snprintf(dest, sz, "%s", value);
  }

  int outwidth() {
    switch (type) {
      case 'S':
        return -width;
      case 'B':
      case 'D':
        return width;
    }
  }

  template <typename T>
  std::string output(T value) {
    char valstr[100];
    char col[100];
    outval(value, valstr, 100);
    int colwidth = width + lpad + rpad;
    if constexpr (std::is_arithmetic_v<T>) {
      snprintf(col, 100, "%*s%*s%*s", lpad, "", outwidth(), valstr, rpad, "");
    } else {
      snprintf(col, 100, "%*s", colwidth, valstr);
    }
    // Enforce max width
    col[colwidth] = '|';
    col[colwidth + 1] = '\0';
    return col;
  }

  std::string name;
  char type;
  int lpad;
  int width;
  int rpad;
};
std::ostream& operator<<(std::ostream& os, ColSpec& spec) {
  os << "ColSpec(" << spec.name << "(" << spec.type << "),"  //
     << "[" << spec.lpad << "/" << spec.width << "/" << spec.rpad << "])";
  return os;
}
auto find_col(const std::string& name) {
  return std::find_if(cols.begin(), cols.end(),
                      [&name](auto& col) { return col.name == name; });
}

int getnum(std::string& str, std::string delim) {
  size_t pos = str.find(delim);
  int val = -1;
  if (pos != std::string::npos) {
    std::string token = str.substr(0, pos);
    token.erase(std::remove(token.begin(), token.end(), ' '), token.end());
    if (token.length()) {
      val = std::stoi(token, nullptr, 2);
    }
    str.erase(0, pos + delim.length());
  }
  return val;
}

void runner_sim(const char* op, char** tokptr, int& cycle) {
  std::cerr << "Running command " << op << "\n";
  if (strncmp(op, "set", 4) == 0) {
    char* port = strtok_r(nullptr, " ", tokptr);
    char* valstr = strtok_r(nullptr, ",;", tokptr);
    char base = 'D';
    if (*valstr == '%') {
      valstr++;
      base = *valstr;
      valstr++;
    }
    std::cerr << "Setting " << port << " to " << valstr << " (" << base << ")"
              << "\n";
    int val = std::stoi(valstr, nullptr, bases[base]);
    cpu->set(port, val);
  } else if (strncmp(op, "eval", 5) == 0) {
    cpu->eval();
  } else if (strncmp(op, "tick", 5) == 0) {
    cycle++;
    cpu->clk = 0;
  } else if (strncmp(op, "tock", 5) == 0) {
    cycle++;
    cpu->clk = 1;
  } else if (strncmp(op, "output", 7) == 0) {
    cpu->eval();
    std::cout << "|";
    for (auto col : cols) {
      std::cerr << "Outputting " << col.name << "\n";
      if (col.name == "time") {
        std::cout << col.output(cycle);
      } else {
        std::visit([&col](auto&& val) { std::cout << col.output(val); },
                   cpu->gets(col.name.c_str()));
      }
    }
    std::cout << "\n";
  }
}

void runner_dump(const char* op, char** tokptr, int& cycle) {
  std::cerr << "Running command " << op << "\n";
  if (strncmp(op, "set", 4) == 0) {
    char* port = strtok_r(nullptr, " ", tokptr);
    char* valstr = strtok_r(nullptr, ",;", tokptr);
    char base = 'D';
    if (*valstr == '%') {
      valstr++;
      base = *valstr;
      valstr++;
    }
    auto col = find_col(port);
    if(col != std::end(cols)) {
      std::cout << port << "=" //
        << col->width << "'" << base << valstr << ";\n";
    }
  } else if (strncmp(op, "eval", 5) == 0) {
    // ignore
  } else if (strncmp(op, "tick", 5) == 0) {
    std::cout << "clk=0;\n";
    std::cout << "#period;\n";
  } else if (strncmp(op, "tock", 5) == 0) {
    std::cout << "clk=1;\n";
    std::cout << "#period;\n";
  } else if (strncmp(op, "output", 7) == 0) {
    // ignore
  }
}

void runner_hex(const char* op, char** tokptr, int& cycle) {
  std::cerr << "Running command " << op << "\n";
  if (strncmp(op, "set", 4) == 0) {
    char* port = strtok_r(nullptr, " ", tokptr);
    char* valstr = strtok_r(nullptr, ",;", tokptr);
    char base = 'D';
    if (*valstr == '%') {
      valstr++;
      base = *valstr;
      valstr++;
    }
    if(std::string(port) == "instruction") {
      int val = std::stoi(valstr, nullptr, bases[base]);
      std::cout << std::hex << val << "\n";
    }
    }
  } else if (strncmp(op, "eval", 5) == 0) {
    // ignore
  } else if (strncmp(op, "tick", 5) == 0) {
    std::cout << "clk=0;\n";
    std::cout << "#period;\n";
  } else if (strncmp(op, "tock", 5) == 0) {
    std::cout << "clk=1;\n";
    std::cout << "#period;\n";
  } else if (strncmp(op, "output", 7) == 0) {
    // ignore
  }
}

int main(int argc, char** argv) {
  Verilated::commandArgs(argc, argv);  // Remember args

  cpu = new HackCPU;  // Create instance

  std::fstream fs;
  fs.open("05/CPU.tst");

  // Just ignore until we hit output-list
  enum State { INIT = 0, INSTR };
  State state{};
  std::string line;
  int cycle = 0;
  for (; std::getline(fs, line);) {
    char* cmdptr;
    char* cmd;
    cmd = strtok_r(line.data(), ",;", &cmdptr);
    while (cmd) {
      char* tokptr;
      char* op = strtok_r(cmd, " ", &tokptr);
      if (!op || op[0] == '\r' || op[0] == '\n') {
        break;
      }
      if (op[0] == '/' && op[1] == '/') {
        break;
      }

      std::cerr << "Executing " << op << "\n";
      switch (state) {
        case State::INIT:
          if (strncmp(op, "output-list", line.size()) == 0) {
            char* colstr;
            std::cout << "|";
            while (colstr = strtok_r(nullptr, " ", &tokptr)) {
              ColSpec spec{colstr};
              std::cerr << "Adding colspec:" << spec << "\n";
              std::cout << spec.output(spec.name);
              cols.push_back(spec);
            }
            std::cout << "\n";
            state = State::INSTR;
          }
          break;
        case State::INSTR:
          runner_sim(op, &tokptr, cycle);
          break;
      }
      cmd = strtok_r(nullptr, ",;", &cmdptr);
    }
  }



  /*
  std::string line;
  std::getline(fs, line);
  size_t linenum = 1;
  for (; std::getline(fs, line);) {
    size_t pos = 0;
    getnum(line, "|");
    alu->x = getnum(line, "|");
    alu->y = getnum(line, "|");
    alu->zx = getnum(line, "|");
    alu->nx = getnum(line, "|");
    alu->zy = getnum(line, "|");
    alu->ny = getnum(line, "|");
    alu->f = getnum(line, "|");
    alu->no = getnum(line, "|");

    int expected_out = getnum(line, "|");
    int expected_zr = getnum(line, "|");
    int expected_ng = getnum(line, "|");

    alu->eval();
    if (alu->out != expected_out) {
      std::cerr << "Mismatch on line " << linenum << "\n";
      std::cerr << "Expected " << expected_out  //
                << ", got " << alu->out;
    } else {
    }

    linenum++;
  }

  vluint16_t x;
  vluint16_t y;
  std::string flags;

  std::cin >> x;
  std::cin >> y;
  std::cin >> flags;
  int flagval = std::stoi(flags, nullptr, 2);

  alu->x = x;  // Set some inputs
  alu->y = y;  // Set some inputs
  alu->zx = flagval & 0b100000;
  alu->nx = flagval & 0b010000;
  alu->zy = flagval & 0b001000;
  alu->ny = flagval & 0b000100;
  alu->f = flagval & 0b000010;
  alu->no = flagval & 0b000001;

  alu->eval();

  std::cout << alu->out << std::boolalpha <<  //
      " (" << bool(alu->zr) << "," << bool(alu->ng) << ")\n";

  alu->final();  // Done simulating
  */
}
