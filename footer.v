module HackGowinRAM1K(data_out, data_in, addr, clk,ce, wre,rst);
  parameter MEM_INIT_FILE = "/home/joel/Projects/nand2tetris/projects/program.hex";
  output [15:0]data_out;
  input [15:0]data_in;
  input [9:0]addr;
  input clk,wre,ce,rst;
  reg [15:0] mem [1023:0];
  reg [15:0] data_out;
  always@(posedge clk or posedge rst)
  if(rst)
    data_out <= 0;
    else
      if(ce & !wre)
      data_out <= mem[addr];

  always @(posedge clk)
  if (ce & wre)
    mem[addr] <= data_in;

  initial begin
    if (MEM_INIT_FILE != "") begin
      $readmemh(MEM_INIT_FILE, mem);
    end
  end
endmodule
