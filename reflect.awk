BEGIN { FS="[ \\(\\),;]+" }
/class.*Register/ {
  gsub(/class /, "#include \"")
  gsub(/;/, ".h\"")
  print
}
/VL_MODULE/ {
  outclass=gensub(/^V/,"",1,$2) 
  print "#include \"verilated.h\""
  print "#include \""$2".h\""
  print "#include <variant>"
  print ""
  print "template<class... Ts> struct overloaded : Ts... { using Ts::operator()...; };"
  print "template<class... Ts> overloaded(Ts...) -> overloaded<Ts...>;"
  print ""
  print "class "outclass": public "$2" {"
  print " public:"
}
/VL_(IN|OUT)/ {
  bits=gensub(/[^0-9]/,"","g",$2)
  dir=gensub(/.*_([^0-9]+).*/,"\\1","g",$2)
  ports[dir][bits]=ports[dir][bits]" "$3
}

/Register\*/ {
  regname=gensub(/.*__/,"",1,gensub(/DOT__([^0-9]+).*/,"\\1",1,$3))
  registers[regname] = $3
}

/^}/ {
  print "  void set(const char* name, int val) {"
  print "    std::cerr << \"Set \" << name << \" to \" << val << \"\\n\";"
  for(bits in ports["IN"]) {
    split(ports["IN"][bits], names, " ")
    for(idx in names) {
      name=names[idx]
      print "    if(strncmp(name, \""name"\","(length(name)+1)") == 0) { "name"=val; return; }"
    }
  } 
  print "    throw;"
  print "  }"
  print "  std::variant<vluint8_t, vluint16_t, vluint32_t, vluint64_t> get(const char* name) {"
  print "    std::cerr << \"Get \" << name << \"\\n\";"
  for(type in ports) {
    for(bits in ports[type]) {
      split(ports[type][bits], names, " ")
      for(idx in names) {
        name=names[idx]
        print "    if(strncmp(name, \""name"\","(length(name)+1)") == 0) { return "name"; }"
      }
    }
  } 
  for(reg in registers) {
    print "    if(strncmp(name, \""reg"[]\","(length(reg)+3)") == 0) { return "registers[reg]"->out; }"
  }
  print "    throw;"
  print "  }"
  print "  using var_s = std::variant<int8_t, int16_t, int32_t, int64_t>;"
  print "  var_s gets(const char* name) {"
  print "    return std::visit(overloaded {"
  split("8 16 32 64",sizes," ")
  for(idx in sizes) {
    size = sizes[idx]
    print "      [](vluint"size"_t val) -> var_s { return static_cast<int"size"_t>(val); },"
  }
  print "    }, get(name));"
  print "  }"
}

END { print "};" }
