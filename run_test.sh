#!/bin/bash
SUCCESS=0
FAIL=0
find $1 -name "*.tst" | while read f; do
    echo -n "$f: "
    if ../tools/HardwareSimulator.sh $f; then
        ((SUCCESS++))
    else
        ((FAIL++))
    fi
done
TOTAL=$(($SUCCESS+$FAIL))
echo "$SUCCESS of $TOTAL tests succeeded"
