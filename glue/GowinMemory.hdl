// This file is part of www.nand2tetris.org
// and the book "The Elements of Computing Systems"
// by Nisan and Schocken, MIT Press.
// File name: projects/05/Memory.hdl

/**
 * The complete address space of the Hack computer's memory,
 * including RAM and memory-mapped I/O.
 * The chip facilitates read and write operations, as follows:
 *     Read:  out(t) = Memory[address(t)](t)
 *     Write: if load(t-1) then Memory[address(t-1)](t) = in(t-1)
 * In words: the chip always outputs the value stored at the memory
 * location specified by address. If load==1, the in value is loaded
 * into the memory location specified by address. This value becomes
 * available through the out output from the next time step onward.
 * Address space rules:
 * Only the upper 16K+8K+1 words of the Memory chip are used.
 * Access to address>0x6000 is invalid. Access to any address in
 * the range 0x4000-0x5FFF results in accessing the screen memory
 * map. Access to address 0x6000 results in accessing the keyboard
 * memory map. The behavior in these addresses is described in the
 * Screen and Keyboard chip specifications given in the book.
 */

CHIP GowinMemory {
    IN in[16], load, address[13],
       buttons[16];
    OUT out[16],
        leds[8], rgbleds[12], digits[12];

    PARTS:
    And(a=load,b=address[12],out=loadboard);
    DMux4Way(in=loadboard,
        a=loadleds,
        b=loadrgb,
        c=loaddlow,
        d=loaddhi,
        sel=address[0..1]);
    Not(in=address[12],out=selram);
    And(a=load,b=selram,out=loadram);

    GowinRAM4K(in=in,out=ram,load=loadram,address=address[0..11]);
    Register(in=in,out=ledsout,out[0..7]=leds,load=loadleds);
    // RGB LEDs
    Register(in=in,out=rgbout,out[0..11]=rgbleds,load=loadrgb);
    // Digits
    Register(in=in,out=dlowout,out[0..11]=digits,load=loaddlow);

    Mux4Way16(
        a=ledsout,
        b=rgbout,
        c=dlowout,
        d=dhiout,
        sel=address[0..1],
        out=boardout);

    Mux16(a=ram,b=boardout,out=out,sel=address[12]);
}
