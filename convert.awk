function translateTrueFalse(wire) {
  return (wire ~ /^true/ ? "~0" :
         (wire ~ /^false/ ? "0" : wire))
}

# Width loader
/WIDTHS_START/ { state="widths"; next }
/WIDTHS_END/ { state="" }
/CLOCKED_START/ { state="clocked"; next }
/CLOCKED_END/ { state="" }
state=="widths" {
  port_widths[$1][$3]=$4
  port_types[$1][$3]=$2
}
state=="clocked" {
  clocked[$0]=1
}

/^\s+\/\// { print; next; }
/^CHIP/ {
  chip=$2
  delete declared
  delete childcount
  print "module Hack"chip"("
  state="modhead"
  if(clocked[chip]) {
    print "input clk,"
  }
  next
}
/IN/ { porttype="input"; delete inout[porttype] }
/OUT/ { porttype="output"; delete inout[porttype] }
/PARTS/ {
  print ");"
  state="modbody"
  next
}
/^}/ {
  print "endmodule"
  print ""
  state = "done"
}
state == "modhead" && length($0) {
  gsub(/(IN|OUT) ?/, "")
  if($0 ~ /^\s+$/) { next }
  patsplit($0, ioports, /[[:alnum:]_]+(\[[0-9]+\])?/, glue)
  # This is to avoid an extra trailing comma, very silly
  num_outputs=0
  for(port in port_types[chip]) {
    if(port_types[chip][port]=="OUT") { num_outputs++ }
  }
  out=glue[0]
  incomment=0
  for(i=1;i<=length(ioports);i++) {
    if(glue[i-1] ~ /\/\//) { incomment=1 }
    if(incomment) { out=out ioports[i] glue[i] }
    else {
      split(ioports[i], parts, /[\[\]]/)
      name=parts[1]
      size=parts[2]
      inout[porttype][name]=1
      if(size) {
        curport="["(size-1)":0] " name
      } else {
        curport=name
      }
      if(porttype == "output" || num_outputs == 0) {
        gsub(/;/,"",glue[i])
      } else {
        gsub(/;/,",",glue[i])
      }
      out=out porttype" "curport glue[i]
    }
  }
  print out
}
state == "modbody" {
  if($0 ~ /(\w+)\(/) {
    indent=(match($0, /\S/)-1)
    partname=gensub(/.*\s+(\w+)\(.*/,"\\1",1,$0)
    partline=NR
    partid=childcount[partname]
    childcount[partname]++
    partstart=gensub(/(\w+)\(.*/,"Hack\\1 \\1"partid" (",1,$0)
  }
  $0 = gensub(/([[:alnum:]\[\.\]_]+)=([[:alnum:]\[\.\]_]+)/,".\\1(\\2)","g")
  $0 = gensub(/([0-9]+)\.\.([0-9]+)/,"\\2:\\1","g")

  # First find any duplicate outputs
  patsplit($0, params, /\.[^\(]+\([^\)]+\)/, glue)
  newports=""
  for(i=1;i<=length(params);i++) {
    split(params[i], parts, /[\.\(\)]/)
    from=parts[2]
    to=parts[3]
    if(from ~ /^[[:alnum:]_]+$/) {
      commwire=from partline
      if(ports[from]) {
        dups[from]=commwire
        dupwires[from]=dupwires[from] "assign "to"="commwire";"
        if(port_widths[partname][from]) {
          wirewidth[commwire]=(port_widths[partname][from]-1)":0"
          wirewidth[to]=(port_widths[partname][from]-1)":0"
        }
      } else {
        ports[from]=translateTrueFalse(to)
        newports=newports" "from
        if(!(to == "true" || to == "false")) {
          dupwires[from]="assign "to"="commwire";"
          basename=gensub(/\[.*/,"",1,to)
          if(!(basename in port_widths[chip])) {
            if(port_widths[partname][from]) {
              wirewidth[basename]=(port_widths[partname][from]-1)":0"
            }
          }
        }
      }
    } else {
      # This is a ranged output, we gotta do weird stuff with it
      range=gensub(/.*\[(.*)\]/,"\\1",1,from)
      basename=gensub(/\[.*/,"",1,from)
      # Disambiguate constant nets, in case we have multiple
      if(to == "true" || to == "false") {
        to=to"_"basename
      }
      # Add it to our map we need to append at the end
      # Pull the dest out of the next glue
      wirewidth[to]=range
      # If we already have the full width of this output mapped,
      # use whatever that's mapped to as the source
      # Otherwise create a temp wire to use
      if(ports[basename]) {
        newport=ports[basename]
      } else {
        # Generate a unique full-width output to split up
        mapno=NR "_" i
        newport="split"mapno
        ports[basename]=newport
        newports=newports" "basename
      }
      if(port_types[partname][basename] == "OUT") {
        wiresource[to]=newport
      } else {
        wiresink[to]=newport
      }
      if(port_widths[partname][basename]) {
        wirewidth[newport]=(port_widths[partname][basename]-1)":0"
      }
    }
  }
  if(newports || NR == partline) { portorder=portorder newports " |" }
}

state == "modbody" && /);$/ {
  # Declare wire widths
  for(wire in wirewidth) {
    if(wire in declared) { continue }
    declared[wire] = 1
    if(wire ~ /\[/) { continue }
    if(wire ~ /^(true|false)_/) { continue }
    split(wirewidth[wire], nums, ":")
    if(nums[2]!="") {
      count = (nums[1] - nums[2])
      # Only declare the wire if it's used, and if it's
      # not already an output
      if(!(inout["output"][wire] || inout["input"][wire])) {
        printf "% "indent"s%s\n", "", "wire [" count ":0] " wire ";"
      }
    }
  }

  # Write out our part
  portlist=""
  gsub(/ \|$/,"",portorder)
  split(portorder,orderedports," ")
  delete filled
  if(partname in port_types) {
    for(p in port_types[partname]) {
      if(port_types[partname][p]=="IN") {
        filled[p]=0
      }
    }
  }
  for(i=1;i<=length(orderedports);i++) {
    from=orderedports[i]
    filled[from]=1
    if(from=="|") {
      portlist=sprintf("%s\n% "indent*2"s",portlist,"")
    } else {
      if(dups[from]) {
        portlist=portlist "."from"("from partline"),"
      } else {
        portlist=portlist"."from"("ports[from]"),"
      }
    }
  }
  if(clocked[partname]) {
    portlist=portlist".clk(clk),"
  }
  for(p in filled) {
    if(filled[p] == 0) {
      portlist=portlist"."p"(0),"
    }
  }
  print partstart gensub(/,$/,");",1,portlist)

  # Assign duplicate mappings
  for(d in dups) {
    if(dups[d]) {
      printf "% "indent"s%s\n", "", dupwires[d]
    }
  }
  portorder=""
  delete ports
  delete dups
  delete dupwires

  # Assign partial mappings
  for(wire in wiresink) {
    printf "% "indent"s%s\n", "", "assign " wiresink[wire] "[" wirewidth[wire] "]="translateTrueFalse(wire)";"
  }
  for(wire in wiresource) {
    printf "% "indent"s%s\n", "", "assign "wire"="wiresource[wire] "[" wirewidth[wire] "];"
  }
  delete wirewidth
  delete wiresource
  delete wiresink
}
